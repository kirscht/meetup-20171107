#!/usr/bin/env bash

#env | sort

[[ -d "google-cloud-sdk/bin/" ]] && export PATH=${PATH}:google-cloud-sdk/bin/

gcloud config set project ${GCR_PROJECT}
gcloud config set compute/zone ${GCR_ZONE}

function createCluster() {

  if [[ $(gcloud container clusters list | egrep "^${GCR_CLUSTER_NAME}" | wc -l) -ge 1 ]]
  then
    echo "cluster is running"
  else
    echo "service is not running"
    gcloud container clusters create ${GCR_CLUSTER_NAME} --password=${GCR_CLUSTER_PASSWORD} --num-nodes=3
  fi

  gcloud container clusters get-credentials ${GCR_CLUSTER_NAME} --zone ${GCR_ZONE} --project ${GCR_PROJECT}

}


###############################
function createDeployment () {
###############################
  set -x

  kubectl get deployments

  transformDeploymentTemplate

  if [[ $(kubectl get deployment ${GCR_SERVICE_NAME} | wc -l ) -gt 1 ]]
  then
    echo "deployment is running"

    kubectl replace -f app-deploy.yml --save-config
  else
    echo "deployment is not running"
    kubectl apply -f app-deploy.yml
  fi

  #kubectl run ${GCR_SERVICE_NAME} --image=gcr.io/${GCR_PROJECT}/${GCR_CONTAINER_NAME}:${DOCKER_TAG} --port=8080 \
  #    --env="myenvironment=${GCR_SERVICE_NAME}"

  kubectl get deployment ${GCR_SERVICE_NAME}
  kubectl get services ${GCR_SERVICE_NAME}

  if [[ $(kubectl get services ${GCR_SERVICE_NAME} | wc -l ) -le 1 ]]
  then
    kubectl expose deployment ${GCR_SERVICE_NAME} --type=LoadBalancer --name=${GCR_SERVICE_NAME}
  fi

}

function transformDeploymentTemplate() {

  cp app-deploy-template.yml app-deploy.yml

  sed -i -e "s/{{DOCKER_TAG}}/${DOCKER_TAG}/g" \
      -e "s/{{GCR_SERVICE_NAME}}/${GCR_SERVICE_NAME}/g" \
      -e "s/{{GCR_REPLICAS}}/${GCR_REPLICAS}/g" \
      app-deploy.yml

  cat app-deploy.yml

  return 0
}

function createProject() {

  if [[ $(gcloud projects list | egrep "^${GCR_PROJECT}"} | wc -l ) -ge 1 ]]
  then
    echo "Project ${GCR_PROJECT} exists"
  else
    echo "Creating project ${GCR_PROJECT}"
    gcloud projects create ${GCR_PROJECT}-987321 --name="${GCR_PROJECT}" \
            --labels=type=happy
  fi

  gcloud projects list
}


cat <<EOD

  Services:
    $(kubectl get services)

  Deployments:
    $(kubectl get deployments)

EOD

if [[ $(expr ${DOCKER_ENV_CI_COMMIT_REF_NAME} : 'version-') -eq 8 ]]
then
  export DOCKER_TAG="$(echo ${DOCKER_ENV_CI_COMMIT_REF_NAME} | cut -f2 -d'-')"
  echo ${DOCKER_TAG} > version.txt
else
  echo "No version change, exiting . . ."
  exit 0
fi

gcloud auth activate-service-account --key-file ${HOME}/gcloud-service-key.json

createProject
createCluster
createDeployment