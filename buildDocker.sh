#!/usr/bin/env bash -x

function dockerinPath() {

  which docker > /dev/null
  return ${?}

}

  if [[ $(dockerinPath) -ne 0 ]]
  then
    echo "no docker in path"
    exit 1
  fi

  cat <<EOD

  GITLAB_CONTAINER_NAME: ${GITLAB_CONTAINER_NAME:=arcusnet}
EOD

  # export DOCKER_ENV_CI_COMMIT_REF_NAME="version-1.0.94"

  if [[ $(expr ${DOCKER_ENV_CI_COMMIT_REF_NAME} : 'version-') -eq 8 ]]
  then
    export DOCKER_TAG="$(echo ${DOCKER_ENV_CI_COMMIT_REF_NAME} | cut -f2 -d'-')"
    echo ${DOCKER_TAG} > version.txt
  else
    echo "No version change, exiting . . ."
    exit 0
  fi

  set -x

  printf "Building Docker Image for %s %s\n" "${GITLAB_CONTAINER_NAME:=arcusnet}" \
                                             "${GCR_PROJECT:=stocks-987321}"
  #docker build -t registry.gitlab.com/kirscht/${GITLAB_CONTAINER_NAME}:${DOCKER_TAG} .
  docker build -t gcr.io/${GCR_PROJECT}/${GCR_CONTAINER_NAME}:${DOCKER_TAG} .

  printf "Testing Docker Image\n"
  docker run gcr.io/${GCR_PROJECT}/${GCR_CONTAINER_NAME}:${DOCKER_TAG} bash /app/tests.sh ; RESULT=${?}

  if [[ ${RESULT} -eq 0 ]]
  then
    printf "pushing Image to Registry\n"
    #docker push registry.gitlab.com/kirscht/${GITLAB_CONTAINER_NAME}:${DOCKER_TAG}

    #docker tag registry.gitlab.com/kirscht/${GITLAB_CONTAINER_NAME}:${DOCKER_TAG} \
    #    gcr.io/${GCR_PROJECT}/${GCR_CONTAINER_NAME}:${DOCKER_TAG}

    gcloud docker -- push gcr.io/${GCR_PROJECT}/${GCR_CONTAINER_NAME}:${DOCKER_TAG}

    #docker tag gcr.io/${PROJECT}/${GCR_CONTAINER_NAME}:${DOCKER_TAG} gcr.io/${GCR_PROJECT}/${GCR_CONTAINER_NAME}:latest
    #google-cloud-sdk/bin/gcloud docker -- push gcr.io/${GCR_PROJECT}/${GCR_CONTAINER_NAME}:latest
  fi

exit 0